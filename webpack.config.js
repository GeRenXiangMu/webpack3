const path=require('path');//绝对路径
const glob = require('glob');//一起来扫描文件时候
const webpack=require('webpack');
const uglify = require('uglifyjs-webpack-plugin');// 压缩插件
const htmlPlugin=require('html-webpack-plugin');//html 模板插件
const extractTextPlugin=require('extract-text-webpack-plugin');//插件的主要是为了抽离css样式,防止将样式打包在js中引起页面样式加载错乱的现象
const  PurifyCSSPlugin=require("purifycss-webpack");//消除未使用的CSS

const entry = require("./webpack_config/entry_webpack.js")//引入外部webpack配置文件
/*dev build 接收参数  */
console.log(encodeURIComponent(process.env.type));

const  copyWebpackPlugin= require("copy-webpack-plugin")//复制静态文件打包

if(process.env.type=="build"){//生产环境
    /*上线的时候用http://jspang.com:1717*/
    var website={
        publicPath:'http://jspang:8080/'

    }
}else{//开发环境
    var website={
        publicPath:'http://192.168.0.165:1717/'
    }
}


module.exports={
    /*devtool: "eval-source-map",*/
    entry:{
      /*  entry:entry.path.entry,*/
        entry:'./src/entry.js',
        jquery:'jquery',
        vue:'vue'
    },/*入口*/
    output:{
        path:path.resolve(__dirname,'dist'),//绝对路径
        filename:'[name].js',// 打包的名字与入口js名字是一样的
        publicPath:website.publicPath
    },/*出口*/
    module: {
        rules:[
            {
                test:/\.css$/,
              /*  loader:['style-loader','css-loader']*/
                use: extractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [
                        {
                            loader:"css-loader",
                            options: {
                             importLoaders:1
                            }
                        },
                        {
                            loader:"postcss-loader"
                        }]
                })
            },
            {
                test:/\.(png|jpg|gif)/,
                use:[{
                    loader: "url-loader", /*url 自带file-loader 所以不用引用*/
                    options:{
                            limit:5000, /*图片最小字节 如果大就直接复制，如果小就转成base64位字节*/
                            outputPath:'images/'
                    }
                }]
            },
            {
                test:/\.(html|htm)$/i,
                use:['html-withimg-loader']
            },
            {
                test: /\.less$/,
                use: extractTextPlugin.extract({
                    use: [{
                        loader: "css-loader"
                    }, {
                        loader: "less-loader"
                    }],
                    // use style-loader in development
                    fallback: "style-loader"
                })
            },
            {
                test: /\.scss$/,
                use: extractTextPlugin.extract({
                    use: [{
                        loader: "css-loader"
                    }, {
                        loader: "sass-loader"
                    }],
                    // use style-loader in development
                    fallback: "style-loader"
                })
            },{
                test:/\.(jsx|js)$/,
                use:{
                    loader:'babel-loader',
                   /* options:{
                        presets:[
                            "es2015","react"
                        ]
                    }*/
                },
                exclude:/node_modules/
            }
        ]
    },
    plugins:[
        //new uglify() 压缩
        new webpack.optimize.CommonsChunkPlugin({ //多入口抽离
            name:['jquery','vue'],
            filename: "assets/js/[name].js",
            minChunks:2
        }),
        new webpack.ProvidePlugin({$:"jquery"}),  //2、通过webpack 配置jquery
        new htmlPlugin({
            minify:{
                removeAttributeQuotes:true
            },
            hash:true,// 每次有缓存 加了hsah 就不会有缓存了
            template:'./src/index.html'
        }),
        new extractTextPlugin("css/index.css"),
        new PurifyCSSPlugin({//消除无用的css文件
            paths:glob.sync(path.join(__dirname,'src/*.html'))
        }),
        new webpack.BannerPlugin('jspang版权所有，看官方免费视频到jspn.com观看'),// 添加版权信息
        new copyWebpackPlugin([{
            from:__dirname+'/src/public',
            to:'./public'
        }]),
        new webpack.HotModuleReplacementPlugin()/*配置热更新*/
    ],
    devServer:{
       contentBase:path.resolve(__dirname,'dist'),
        host:'192.168.0.165',
        compress:true,//服务器压缩
        port:1717
    },
    watchOptions: {
        poll:1000,// watch 1 秒后自动打包
        aggregateTimeout:500,// 防止重复按键 打包报错
        ignored:/node_modules/,//不需要监测的文件
    }
}